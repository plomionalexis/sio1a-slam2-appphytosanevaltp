package data;
import entites.Exploitation;
import entites.Parcelle;
import entites.Produit;
import java.util.List;

public class Dao {
    
    public static Exploitation       getLExploitation(String idExp ) {
    
       Exploitation exp=null;
      
      for (Exploitation e : getToutesLesExploitations()){
      
        if(e.getIdExp().equals(idExp)) exp=e;
      }
      
      return exp;
    
    }
    public static List<Exploitation> getToutesLesExploitations()     {return Donnees.lesExploitations;} 

    public static Parcelle           getLaParcelle(String idParc)    {
    
      Parcelle p=null;
      
      for (Parcelle parc : getToutesLesParcelles()){
      
        if(parc.getIdParc().equals(idParc)) p=parc;
      }
      
      return p;
    
    }   
    public static List<Parcelle>     getToutesLesParcelles()         {
    
      return Donnees.lesParcelles;
    } 
    
    public static Produit            getLeProduit(String pIdProd)    {
     
     
      for(Produit prod : getTousLesProduits()){
      
         if( prod.getIdProd().equals(pIdProd)) return prod;
      }
      return null;
    
    }  
    public static List<Produit>      getTousLesProduits()            {
        return Donnees.lesProduits;
    }
}
