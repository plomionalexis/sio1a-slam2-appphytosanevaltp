package entites;

import java.util.LinkedList;
import java.util.List;

public class Produit {
    
    private String           idProd;
    private String           nomprod;
    
    private List<Traitement> lesTraitements;

    public Produit() {
    
      lesTraitements= new LinkedList();
    }

    public Produit(String idProd, String nomprod) {
        
      this();
      this.idProd = idProd;
      this.nomprod = nomprod;
    }

    //<editor-fold defaultstate="collapsed" desc="Gets & Sets">
    
    public List<Traitement> getLesTraitements() {
        return lesTraitements;
    }
    
    public void setLesTraitements(List<Traitement> lesTraitements) {
        this.setLesTraitements(lesTraitements);
    }
    
    
    public String getIdProd() {
        return idProd;
    }

    public void setIdProd(String idProd) {
        this.idProd = idProd;
    }

    public String getNomprod() {
        return nomprod;
    }

    public void setNomprod(String nomprod) {
        this.nomprod = nomprod;
    }

    //</editor-fold>
}



