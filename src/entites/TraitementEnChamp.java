package entites;
import java.util.LinkedList;
import java.util.List;

public class TraitementEnChamp  extends Traitement{
 
    private List<Pulverisation> lesPulverisations;

    public TraitementEnChamp( ) {
        this.lesPulverisations = new LinkedList();
    }

    public TraitementEnChamp(String idTrait, Produit leProduit, Parcelle laParcelle) {
       
        super(idTrait, leProduit, laParcelle);
        this.lesPulverisations = new LinkedList();
        
    }

    
    // quantiteAppliquee
    // Un traitement en champ comporte plusieurs pulverisations
    // A chaque  pulvérisation le poids appliqué s'obtient
    // en multipliant le dosage de la pulverisation ( en kg par  hectare)
    // par la surface de la parcelle concernée ( en hectares ) 
    
    // la quantité à retourner est donc la somme de tous ces poids
    
    @Override
    public Float quantiteAppliquee() {
        float quantite = 0f;
        
        for(Pulverisation pulv : lesPulverisations){
            quantite +=pulv.getDosagePulv() * this.getLaParcelle().getSurface();
        }
        
        
        
        return quantite;
        
        
    }

    //<editor-fold defaultstate="collapsed" desc="Gets & Sets">
    
    public List<Pulverisation> getLesPulverisations() {
        return lesPulverisations;
    }
    
    public void setLesPulverisations(List<Pulverisation> lesPulverisations) {
        this.lesPulverisations = lesPulverisations;
    }
    //</editor-fold>

    
    // Affichage du produit utilisé et de la liste des pulvérisation
    @Override
    public void afficher() {
    
         System.out.print("Traitement en champ ");
                
         System.out.printf("Produit %-10s\n", getLeProduit().getNomprod());
                 
         System.out.println("\n  Pulvérisations:\n");
         for (Pulverisation pulv : getLesPulverisations()){
                 
             System.out.printf(
                     
             "  le %-8s %6.2f kg\n",      
             utilitaires.UtilDate.dateVersChaine(pulv.getDatePulv()),
             pulv.getDosagePulv()* getLaParcelle().getSurface()
             ); 
         }
                   
         System.out.printf(
          
             "\n Quantité totale appliquée: %6.2f kg\n",
             quantiteAppliquee()
         );
    }
}


