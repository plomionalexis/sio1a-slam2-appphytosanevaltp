package progs;
import entites.Exploitation;
import entites.Parcelle;
import java.util.Scanner;

public class P01 {

    public static void main(String[] args) {
        
        Scanner clavier= new Scanner(System.in);
        
        
        // NB: Pour vos tests, les id d'exploitation du jeu d'essais sont exp001 et exp002
        
        System.out.println("Identifiant de l'exploitation? ");
        
        String identExploit = clavier.next();
                
        Exploitation exploitation=  data.Dao.getLExploitation(identExploit);
        
        System.out.printf("\n Exploitant: %-25s \n\n", exploitation.getNomExploitant());
        System.out.printf("Adresse : %-25s \n",exploitation.getAdrExploitant());
        System.out.printf("Surface : %-25s \n",exploitation.surfaceTotaleDesParcelles());
        System.out.println("");
        for(Parcelle p : exploitation.getLesParcelles())
        {
            System.out.println("Surface : "+p.getSurface()+ "hectares");
            System.out.println("Espèce : "+p.getlEspeceCultivee().getNomEsp());
            System.out.println("Date semance : "+p.getDateSemis());
            System.out.println("Date de récolte prévue : "+p.getDateRecoltePrevue());
            System.out.println("");
        }
       
    }
}






